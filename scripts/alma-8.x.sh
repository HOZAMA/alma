#!/bin/bash

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Prepares a Red Hat Enterprise Linux 8 derivative guest operating system.

export BUILD_USERNAME
export BUILD_KEY

### Update the default local user. ###
echo '> Updating the default local user ...'
echo '> Adding the default local user to passwordless sudoers...'
sudo bash -c "echo \"$BUILD_USERNAME ALL=(ALL) NOPASSWD:ALL\" >> /etc/sudoers"

### Disable SELinux. ### 
echo '> Disabling SELinux ...'
sudo sed -i 's/^SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

### Create the clean script. ###
echo '> Creating the clean script ...'
sudo cat <<EOF > /home/$BUILD_USERNAME/clean.sh
#!/bin/bash

###  Cleans all audit logs. ### 
echo '> Cleaning all audit logs ...'
if [ -f /var/log/audit/audit.log ]; then
cat /dev/null > /var/log/audit/audit.log
fi
if [ -f /var/log/wtmp ]; then
cat /dev/null > /var/log/wtmp
fi
if [ -f /var/log/lastlog ]; then
cat /dev/null > /var/log/lastlog
fi

### Cleans persistent udev rules. ### 
echo '> Cleaning persistent udev rules ...'
if [ -f /etc/udev/rules.d/70-persistent-net.rules ]; then
rm /etc/udev/rules.d/70-persistent-net.rules
fi

### Clean the /tmp directories. ###
echo '> Cleaning /tmp directories ...'
rm -rf /tmp/*
rm -rf /var/tmp/*
rm -rf /var/cache/dnf/*

### Clean the SSH keys. ###
echo '> Cleaning the SSH keys ...'
rm -f /etc/ssh/ssh_host_*

### Set the hostname to localhost. ###
echo '> Setting the hostname to localhost ...'
cat /dev/null > /etc/hostname
hostnamectl set-hostname localhost

### Clean dnf cache. ###
echo '> Cleaning dnf cache ...'
dnf clean all

### Clean the machine-id. ###
echo '> Cleaning the machine-id ...'
truncate -s 0 /etc/machine-id
rm /var/lib/dbus/machine-id
ln -s /etc/machine-id /var/lib/dbus/machine-id

### Clean the shell history. ###
echo '> Cleaning the shell history ...'
unset HISTFILE
history -cw
echo > ~/.bash_history
rm -fr /root/.bash_history

### Prepare cloud-init ###
echo '> Preparing cloud-init ...'
rm -fr /etc/cloud/cloud-init.disabled
EOF

### Change the permissions on /tmp/clean.sh. ###
echo '> Changing the permissions on /tmp/clean.sh ...'
sudo chmod +x /home/$BUILD_USERNAME/clean.sh

### Run the cleau script. ### 
echo '> Running the clean script ...'
sudo /home/$BUILD_USERNAME/clean.sh

### Done. ### 
echo '> Done.'